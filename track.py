#!/usr/bin/python
# -*- encoding: utf-8 -*-

class Track(object):
    """General track info."""
    
    def __init__(self, name=None, artist=None, album=None):
        self.name = name or ''
        self.artist = artist or ''
        self.album = album or ''
