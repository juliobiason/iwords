#!/usr/bin/python

import itunes
import optparse

def display_words(l):
	words = l.items()
	words.sort(key=lambda word: word[1], reverse=True)
	for pos in xrange(min(10, len(words))):
		print words[pos][0], words[pos][1]
	return

def make_options():
	parser = optparse.OptionParser()
	general = optparse.OptionGroup(
		parser,
		'General options')

	general.add_option('-i', '--itunes',
		action='store_const',
		const='itunes',
		dest='parser',
		help='Parse iTunes playlist')

	parser.add_option_group(general)

	itunes = optparse.OptionGroup(parser,
		'iTunes options')
	itunes.add_option('--itunes-playlist-file',
		dest='itunes_playlist',
		action='store',
		default=None,
		help='Path to iTunes playlist file. ' \
			'Default is ~/Music/iTunes/iTunes Music Library.xml')
	
	parser.add_option_group(itunes)

	return parser

def main():
	opts = make_options()
	(options, args) = opts.parse_args()
	if not options.parser:
		opts.error('Select one playlist to be parsed')
		return

	if options.parser == 'itunes':
		playlist = itunes.iTunes()

	
	songs = {}	# to avoid duplicate song names
	artist_words = {}
	words_count = {}

	for track in playlist.tracks():
		artist = track.artist.lower()
		name = track.name.lower()

		if not artist in songs:
			songs[artist] = []

		if not artist in artist_words:
			artist_words[artist] = {}

		if not name in songs[artist]:
			songs[artist].append(name)
			words = name.split()

			for word in words:
				word = word.strip(' ,-[]()?"!.:')
				if not word:
					continue

				if word in ['the', 'a', 'to', 'of']:
					continue

				artist_count = artist_words[artist].get(word, 0)
				artist_words[artist][word] = artist_count + 1

				count = words_count.get(word, 0)
				words_count[word] = count + 1

	display_words(words_count)
	print

	for artist in artist_words:
		print artist
		display_words(artist_words[artist])
		print
	return
	
if __name__ == '__main__':
	main()