#!/usr/bin/python
# -*- encoding: utf-8 -*-

import plistlib
import os.path
import track

class iTunes(object):
    """Parser of iTunes playlists."""

    def __init__(self):
    	self._filename = os.path.expanduser('~/Music/iTunes/iTunes Music Library.xml')

    def tracks(self):
    	content = plistlib.readPlist(self._filename)
    	for id in content['Tracks']:
    		trackInfo = content['Tracks'][id]
    		yield track.Track(
    				trackInfo.get('Name'),
    				trackInfo.get('Artist'),
    				trackInfo.get('Album'))